<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    public function get_create_new_task_route()
    {
        return route('tasks.store');
    }

    public function get_create_task_view_route()
    {
        return route('tasks.create');
    }

    public function get_login_route()
    {
        return route('login');
    }

    /** @test*/
    public function authenticated_user_can_create_new_task()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->make()->toArray();

        $response = $this->post($this->get_create_new_task_route(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test*/
    public function unauthenticated_user_can_not_create_new_task()
    {

        $task = Task::factory()->make()->toArray();

        $response = $this->post($this->get_create_new_task_route(), $task);
        $response->assertRedirect($this->get_login_route());
    }

    /** @test */
    public function authenticated_user_can_not_create_task_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();

        $response = $this->post($this->get_create_new_task_route(), $task);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_can_create_view_task()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->get_create_task_view_route());
        $response->assertViewIs('tasks.create');
    }

    /** @test */
    public function authenticated_user_can_see_name_required_text_if_validate_err()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();

        $response = $this->from($this->get_create_task_view_route())->post($this->get_create_new_task_route(), $task);
        $response->assertRedirectToRoute('tasks.create');
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_task_form_view()
    {
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->get_create_task_view_route())->post($this->get_create_new_task_route(), $task);

        $response->assertRedirect('/login');
    }
}
