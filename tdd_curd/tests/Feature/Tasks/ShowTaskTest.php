<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    public function getTaskRoute($id)
    {
        return route('tasks.show', $id);
    }
    /** @test */
    public function user_can_see_task_if_task_exits(): void
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getTaskRoute($task->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee($task->name);
    }

    /** @test */
    public function user_can_not_see_task_if_task_does_not_exits(): void
    {
        $taskId = -1;
        $response = $this->get($this->getTaskRoute($taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
