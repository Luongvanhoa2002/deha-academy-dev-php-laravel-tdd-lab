<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    public function test_user_can_get_list_task()
    {
        $tasks = Task::all();
        $respone = $this->get(route('tasks.index'));
        $respone->assertStatus(Response::HTTP_OK);
        $respone->assertViewIs('tasks.index');
        $respone->assertViewHas('tasks', $tasks);
    }
}
