@extends('layouts.app')
@section('name', 'Task Edit')
@section('content')
<div class="container">
    <div class="row">
        <h2 class="">Task Edit</h2>
        <form action="{{ route('tasks.update', [ 'task' => $task->id ]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $task->name }}">
                @error('name')
                    <div class="form-text text-danger">The name field is required.</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">Content</label>
                <textarea type="" class="form-control" id="name" name="content" >{{ $task->content }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

