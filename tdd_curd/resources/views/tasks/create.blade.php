@extends('layouts.app')
@section('name', 'Task Create')
@section('content')
<div class="container">
    <div class="row">
        <h2 class="">Create task</h2>
        <form action="{{ route('tasks.store')}}" method="POST">
            @method('POST')
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" name="name">
                @error('name')
                    <div class="form-text text-danger">The name field is required.</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">Content</label>
                <input type="text" class="form-control" id="name" name="content">
            </div>
            <button type="submit" class="btn btn-primary mx-2">Submit</button>
            <a href="{{ route('tasks.index') }}" class="btn btn-secondary">Back</a>
        </form>
    </div>
</div>
@endsection

