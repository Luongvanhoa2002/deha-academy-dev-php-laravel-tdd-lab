@extends('layouts.app')
@section('name', 'Tasks List')
@section('content')
<div class="container">
    <nav class="navbar bg-body-tertiary">
        <div class="container-fluid">
            <a href="{{ route('tasks.create') }}" class="btn btn-success"> Create Task</a>
            <form class="d-flex" action="{{ route('tasks.search') }}" method="GET">
                <input class="form-control me-2" type="text" name="keyword" value="{{ isset($keyword) ? $keyword : '' }}" placeholder="Search">
                <button class="btn btn-outline-success" type="submit" >Search</button>
            </form>
        </div>
    </nav>
    <table class="table table-striped" id="table-data">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Content</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tasks as $key => $task)
            <tr>
                <td>{{ $task->id }}</td>
                <td>{{ $task->name }}</td>
                <td>{{ $task->content }}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{ route('tasks.show', ['task' => $task->id])}}" class="btn btn-info mx-2"> Show </a>
                        <a href="{{ route('tasks.edit', ['task' => $task->id])}}" class="btn btn-primary mx-2"> Edit </a>
                        <form action="{{ route('tasks.destroy', ['task' => $task->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"> Delete </button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

