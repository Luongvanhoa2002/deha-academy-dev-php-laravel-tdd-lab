@extends('layouts.app')
@section('name', 'Task Show')
@section('content')
<div class="container">
    <div class="row">
        <h2 class="">Show task</h2>
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" readonly value="{{ $task->name }}" name="name">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Content</label>
            <input type="text" class="form-control" id="name" readonly value="{{ $task->content}}" name="content">
        </div>
        <a href="{{ route('tasks.index') }}" class="btn btn-secondary">Back</a>
    </div>
</div>
@endsection

